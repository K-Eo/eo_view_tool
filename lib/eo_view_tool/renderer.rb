module EoViewTool
  class Renderer
    def self.copyright target, msg
      "&copy; #{Time.now.year} | <b>#{target}</b> #{msg}".html_safe
    end
  end
end